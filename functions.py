import unittest
import sys
import csv
from csv import reader
import pandas as pd
import os.path as path
import parser
import os



# To check whether the file passed is a csv or not.
def check_if_csv(filepath):
    path = filepath.replace("./fixtures/","")    
    extension = path[-4:]
    if(extension == ".csv"):
        return True
    else:
        return False


def print_csv_to_stdout(length_of_arguments):
    # Initializing the number of expected rows
    no_of_expected_rows = 0
    # Initializing the number of actual rows
    no_of_actual_rows = 0
    # Iterating the INPUT CSV files, one-by-one.
    for x in range(1, length_of_arguments):
        # Working on each CSV file, and writing it in the STDOUT row by row.
        with open(sys.argv[x], 'r') as read_obj:
            # Fetch the entire path where the CSV is residing.
            cr = sys.argv[x]
            # Fetch CSV Filename
            filename_csv = cr.replace("./fixtures/","")
            # Create a csv.reader object from the input file object
            csv_reader = reader(read_obj)
            # Converting the object to a list
            csv_reader = list(csv_reader)
            # Calculating the number of rows.
            no_of_expected_rows = no_of_expected_rows + len(csv_reader)
            # Extracting each row and printing to stdout.
            # Reading each row, and printing it to stdout.
            no_of_actual_rows = no_of_actual_rows + read_each_row_to_stdout(csv_reader,filename_csv)
    return no_of_expected_rows, no_of_actual_rows


def read_each_row_to_stdout(csv_reader,filename_csv):
    rows_initialization = 0
    # Read each row of the input csv file as list
    for y in range(1, len(csv_reader)):
        # Append the filename in the row / list
        csv_reader[y].append(filename_csv)
        # Code to remove ( \ ) and ( " ) in the fields. This is the sanitisation step.
        # Commented here since, it was not asked in the first place.
        # I believe this step could consume more time, 
        # And I don't want to mess up any time or memory limits.
        '''if('\\' in csv_reader[y][1]):
            csv_reader[y][1] = csv_reader[y][1].replace("\\", "")
            csv_reader[y][1] = csv_reader[y][1].replace('"', "")'''
        # Add the updated row / list to the output file
        string = csv_reader[y][0]+","+csv_reader[y][1]+","+csv_reader[y][2]
        # Counting the rows
        rows_initialization = rows_initialization + 1
        sys.stdout.write(string+"\n")
    return rows_initialization