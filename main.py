import unittest
import sys
import csv
from csv import reader
import pandas as pd
import os.path as path
import parser
import os
import functions


class Testcsv(unittest.TestCase):

    # Test to check if all the input files are csv or not.
    def test_csv_or_not(self):
        # Before we start anything,
        # Test if the input files are csv files,
        # And, if not, the test fails.
        self.assertEqual(match_flag, 0)

    # Test to check if the column names are equal in all the files.
    def test_columns_in_each_csv(self):
        # Let's check if the column names in all csv files are same.
        column_names = ["email_hash", "category"]
        z = 0
        for x in range(1, length_of_arguments):
            # Fetch the header column.
            df = pd.read_csv(sys.argv[x], nrows=1).columns
            # Check if they are matching with Column_Names
            if(column_names[0] == str(df[0]) and column_names[1] == str(df[1])):
                z = z+1
            else:
                temp = -1
                self.assertEqual(z, temp)
        self.assertEqual(z, length_of_arguments - 1)

    # Test to check all the csv files to a combined csv with same number of rows.
    def test_csv_to_stdout(self):
        # Before we start anything,
        # Test if the input files are csv files,
        # And, if not, the test fails.
        no_of_expected_rows,no_of_actual_rows = functions.print_csv_to_stdout(length_of_arguments)
        # Calculating the rows, and the expected rows. And, passing them through 
        total_arguments = length_of_arguments - 1
        self.assertEqual(no_of_actual_rows, no_of_expected_rows - total_arguments)
        
if __name__ == '__main__':
    # Encoding the file with UTF-8 format.
    sys.stdout.reconfigure(encoding='utf-8')
    # Starting the first row, in the STDOUT.
    sys.stdout.write("email_hash,category,filename\n")
    # Take the length of arguments
    length_of_arguments = len(sys.argv)
    match_flag = length_of_arguments - 1
    # Iterating through all the CSV Files, and check if they are CSV files.
    for x in range(1, length_of_arguments):
        result = functions.check_if_csv(sys.argv[x])
        if(result):
            match_flag = match_flag - 1
    # When the match_flag is 0, it means all the input files are csv files.
    unittest.main(argv=['first-arg-is-ignored'], exit=False)

