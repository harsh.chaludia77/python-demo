import unittest
import sys
import csv
from csv import reader
import pandas as pd
import parser
import os
import functions
import glob

class UnitTests(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        csv_list = []
        for files in glob.glob("./fixtures/*.*"):
            csv_list.append(files)
        cls.csv_list = csv_list
        cls.length_of_arguments = len(cls.csv_list)
        match_flag = cls.length_of_arguments
        # Iterating through all the CSV Files, and check if they are CSV files.
        # When the match_flag is 0, it means all the input files are csv files.
        for x in range(0, cls.length_of_arguments):
            result = functions.check_if_csv(cls.csv_list[x])
            if(result):
                match_flag = match_flag - 1
        cls.match_flag = match_flag 

    # Test to check if all the input files are csv or not.
    def test_csv_or_not(self):
        # Before we start anything,
        # Test if the input files are csv files,
        # And, if not, the test fails.
        self.assertEqual(0, self.match_flag)

    # Test to check if all column names have same names in all the files.
    def test_if_columns_have_names_same(self):
        # Let's check if the column names in all csv files are same.
        column_names = ["email_hash", "category"]
        z = 0
        for x in range(0, self.length_of_arguments):
            # Fetch the header column.
            df = pd.read_csv(self.csv_list[x], nrows=1).columns
            # Check if they are matching with Column_Names
            if(column_names[0] == str(df[0]) and column_names[1] == str(df[1])):
                z = z+1
        self.assertEqual(z, self.length_of_arguments)

    # Test to check if all CSVs have equal number of columns.
    def test_if_csv_have_equal_columns(self):
        # Store the length of columns in each csv.
        z = []
        for x in range(0, self.length_of_arguments):
            # Fetch the header column.
            df = pd.read_csv(self.csv_list[x], nrows=1).columns
            z.append(len(df))
        # Check if every CSV has equal number of columns.
        boolean_value = all(x == z[0] for x in z)
        self.assertEqual(True, boolean_value)

    # Test to check if all CSVs have sanitised fields (without [ / ] or [ " ].
    def test_if_csv_has_unsanitised_fields(self):
        # Read each row of the input csv file as list
        boolean_value = True
        for x in range(0, self.length_of_arguments):
            data = pd.read_csv(self.csv_list[x]) 
            # Find all the fields where you see [ \ ]
            get_values_backslashes = data['category'].str.contains(pat='\\ ')
            # Find all the fields where you see [ " ]
            get_values_double_inverted_commas = data['category'].str.contains(pat='"')
            if((True in get_values_backslashes) and (True in get_values_double_inverted_commas)):
                boolean_value = False
        self.assertEqual(False, boolean_value)


if __name__ == '__main__':
    unittest.main()