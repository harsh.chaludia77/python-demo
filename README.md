# CSV Combiner

The CSV Combiner program has been written in python, and I have several unit tests implemented on combining the CSV.
The program also uses a CI/CD pipeline supported by Gitlab. The YML file has been attached with the submission.

There are two files for combining the CSVs, one being the main.py where all the unit tests along with some functions being imported from another file, and other being the functions.py where all the functions have been drafted.

## Input & Output
Run the code as follows
```
$ python main.py ./fixtures/accessories.csv ./fixtures/clothing.csv ./fixtures/household_cleaners.csv > combined.csv
```

## Unit Tests

Check if all the files are csv or not
```
$ python -m unittest tests.UnitTests.test_csv_or_not
```

Check if all the header columns in CSV are same.
```
$ python -m unittest tests.UnitTests.test_if_columns_have_names_same
```

Check if all the csv has equal number of columns.
```
$ python -m unittest tests.UnitTests.test_if_csv_have_equal_columns
```

Check if you require santisation in your data.
```
$ python -m unittest tests.UnitTests.test_if_csv_has_unsanitised_fields
```


## Files

<b>Main.py</b> => This file can take dynamic arguments with CSV files. And, performs three unit tests, followed by processing the combined CSV. <br/>
<b>functions.py</b> => This file includes drafted functions, which are used in Main.py, and tests.py.<br/>
<b>tests.py</b> => This file performs UNIT TESTs using the python UNITTEST module.<br/>

## Example

Given two input files named `clothing.csv` and `accessories.csv`.
In this case, the execution code would be,
```
$ python main.py ./fixtures/accessories.csv ./fixtures/clothing.csv > combined.csv
```

|email_hash|category|
|----------|--------|
|21d56b6a011f91f4163fcb13d416aa4e1a2c7d82115b3fd3d831241fd63|Shirts|
|21d56b6a011f91f4163fcb13d416aa4e1a2c7d82115b3fd3d831241fd63|Pants|
|166ca9b3a59edaf774d107533fba2c70ed309516376ce2693e92c777dd971c4b|Cardigans|

|email_hash|category|
|----------|--------|
|176146e4ae48e70df2e628b45dccfd53405c73f951c003fb8c9c09b3207e7aab|Wallets|
|63d42170fa2d706101ab713de2313ad3f9a05aa0b1c875a56545cfd69f7101fe|Purses|

Your script would output

|email_hash|category|filename|
|----------|--------|--------|
|21d56b6a011f91f4163fcb13d416aa4e1a2c7d82115b3fd3d831241fd63|Shirts|clothing.csv|
|21d56b6a011f91f4163fcb13d416aa4e1a2c7d82115b3fd3d831241fd63|Pants|clothing.csv|
|166ca9b3a59edaf774d107533fba2c70ed309516376ce2693e92c777dd971c4b|Cardigans|clothing.csv|
|176146e4ae48e70df2e628b45dccfd53405c73f951c003fb8c9c09b3207e7aab|Wallets|accessories.csv|
|63d42170fa2d706101ab713de2313ad3f9a05aa0b1c875a56545cfd69f7101fe|Purses|accessories.csv|

##  Important
* The code is neatly written with comments, and explainations. The code is re-usable and extensible.
* The code is testable by a CI/CD process. Unit tests are mentioned in the system.


## Gitalb CI/CD Pipeline

![alt text](pipeline.PNG)